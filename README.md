# これはなに? #

このリポジトリは、Linuxの授業用環境構築を支援するansible playbookです。
といっても自分の授業向けにしているのでかなりアレです。

# 使い方 #

ダウンロードしたらunzipで展開して、`ansible-playbook` で実行してください。
といっても面倒なので、`make`でバッチ実行できます。

もしくは普通にリモートから流し込むことも可能です。
こちらのほうがパッケージの余計な準備が(当面は)不要なので、容量軽くなるかも。

# 必要なもの #

ローカル実行させるためには、以下のものが用意されている必要があります。

* pip経由で
  * ansible
  * markupsafe(ansibleが依存)
* パッケージで
  * git(任意) --- こっちならfetch/mergeかpullで更新できて便利かな
  * build-essential
  * virtualbox-guest-dkms
  * python-dev

> $ sudo apt-get update
> $ sudo apt-get install -y virtualbox-guest-dkms python-pip python-dev

リモート実行させるためには、以下のものが用意されている必要があります。

* 制御側
  * ansibleとそれを動かす環境
* リモート側(VM)
  * sshでつながること
  * python
  * といっても、配布VMは基本的に入ってます

# その他注意事項 #

* VMの仕様
  * 512MB RAM
  * 1 CPU
  * ユーザとしてlinux(パスワードpenguin)、sudo権限あり(パスワード必要)
  * vim 鬼教官モードを適用 http://bit.ly/1D5s85h

# ざっくりな手順 #

自動インストール手順を用いている場合はすでに準備済みなので、playbookのダウンロードからでOKです。

1. ansibleを入れる(pip)

  > sudo apt-get install python-pip python-dev
  > sudo pip install ansible

2. playbookを取り出す

  > cd /tmp
  > git clone https://bitbucket.org/densuke/linux-vmconfig.git
  > cd linux-vmconfig
  > ansible-playbook -i hosts -K config.yml

この際、linuxユーザのパスワードを聞かれます(sudo用)。入力すればあとは必要な処理をほぼ勝手に行います。


# リリースノート #

* v0.8.4

  * 利用するシェルがデフォルトのまま(環境によりbsh)になっていたためbashを明示するようにした

* v0.8.3

  * fstabを修正: FSCK回避

* v0.8.1

  * システム更新時の余波でタイムゾーンが元に戻る問題に対応

* v0.8

  * タイムゾーンを日本向に調整するコードを追加
  * sudoのラッパーを登録し、学内無線を利用するときにプロキシを利用するようにした

* v0.7

  * リポジトリミラーに国外のものがあったときに日本のミラーに変更するようにしました
  * studyvmの仕様に合わせて処理ユーザをlinuxにしました
  * pam_mkhomedir.soによりホームディレクトリを初回ログイン時に作るようにしました
  * vi鬼教官: chattrによる保護設定を、より確実に行うようにしました
  * システム更新を最後にかけて、autoremove処理で余計なものがアレば削除するようにしました
  * その他バグフィックス

* v0.6

  * 権限付与に問題があったので修正

* v0.5

  * 基本機能の投入
